Feature: Explore test HomePage JobHop web application

  @
  Scenario: 01_As a user, I explore test Jobhop home page
    Given I am on the jobhop home page
    When I login with <username> and <password>
    Then I should see a flash message saying <message>
