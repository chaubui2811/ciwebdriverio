Feature: Explore test HomePage Kiwimi Admin web application

  Scenario: 01_As a user, I explore test Kiwimi Admin page
    Given I am on the Kiwimi Admin page
    When I login with admin@user.local and 123456
    Then I should see a flash message saying Welcome to Administrator