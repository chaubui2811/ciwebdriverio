import { Given, When } from "@wdio/cucumber-framework";
import DropDownListPage from "../pageObjects/dropDownListPage/dropDownListPage";

Given(/^I am on ToolQA\/dropdown list page$/, async () => {
  await DropDownListPage.goTo("https://demoqa.com/select-menu");
});
When(/^I test dropdown$/, async () => {
  await DropDownListPage.selectOldStyleDropdownList();
});
