import { Given, When, Then } from "@wdio/cucumber-framework";
import supertest from "supertest";
import header from "../testData/header.json";
import authed from "../testData/authen.json";
import body from "../testData/body.json";

const baseUri = "https://www.google.com";
const request = supertest(baseUri);
let response: supertest.Response;
let api_statusCode: number;
let api_resBody: string;

const payload = {
  name: "test",
  job: "tester",
};

const bodies = JSON.stringify({
  operationName: "GetAllUserTransactions",
  variables: {
    page: 1,
    pageSize: 20,
    order_by: {
      createdDatetime: "DESC",
    },
    where: {
      createdDatetime_gte: 1655485200000,
      createdDatetime_lte: 1658163599000,
    },
  },
  query:
    "query GetAllUserTransactions($csvHeaders: [String], $export: Boolean, $order_by: UserTransactionDtoSort, $page: Int, $pageSize: Int, $where: UserTransactionDtoFilter) {\n  userTransactions(csvHeaders: $csvHeaders, export: $export, order_by: $order_by, page: $page, pageSize: $pageSize, where: $where) {\n    totalCount\n    edges {\n      createdDatetime\n      expectedAmount\n      transactionId\n      paidAmount\n      paidDatetime\n      paymentNotes\n      paymentTransferNumber\n      paymentUnit\n      productName\n      transactionId\n      transactionPaymentStatus\n      transactionServiceType\n      transactionType\n      updatedDatimetime\n      userId\n      userTransactionId\n      propertyPostId\n      __typename\n    }\n    __typename\n  }\n}\n",
});

const commonHeaders = {
  Authorization:
    "Bearer eyJhbGciOiJQUzI1NiIsImtpZCI6IjE5ZGZiNmE0ZTVhNzQ2MDNiZmEzZmI5MmFjYzIyMzBlIiwidHlwIjoiYXQrand0In0.eyJuYmYiOjE2NTgxNTg3OTYsImV4cCI6MTY1ODE2MjM5NiwiaXNzIjoiaHR0cHM6Ly9pZC1zdGcudG9wZW5sYW5kLmNvbSIsImF1ZCI6WyJncmFwaHFsLWdhdGV3YXkiLCJjb21tdW5pY2F0aW9uIiwicGVyc29uYWwiLCJzYWxlIiwicG9zdCIsInN1cHBvcnQtcmVxdWVzdCIsImNvbnRlbnQiLCJtYXN0ZXItZGF0YSIsInRyYW5zYWN0aW9uIiwiSWRlbnRpdHlTZXJ2ZXJBcGkiLCJjb250YWN0LXRyYWRpbmciLCJzdWJzY3JpcHRpb24iLCJjb250cmFjdCIsImltYWdlIiwiZmlsZSIsInBhcnRuZXJzIiwic29jaWFsLW5ldHdvcmsiLCJkb2N1bWVudCIsInN1cHBvcnQtc2VydmljZSIsImNyYXdsZXIiLCJwYXltZW50IiwiYzJjLWNvbnRhY3QtdHJhZGluZyJdLCJjbGllbnRfaWQiOiJrbEs4VEN4RTBFYkhDT1prbkIiLCJzdWIiOiI3Y2E2ODRmNC0xZjNmLTRkYTItOWFkOS0wZDllODFkN2E0NDQiLCJhdXRoX3RpbWUiOjE2NTgxNTg3OTYsImlkcCI6ImxvY2FsIiwicm9sZSI6ImFnZW50IiwicGVybWlzc2lvbiI6WyIxMC5mIiwiMTEuZiIsIjEyLnYiLCIxMy52IiwiMTQuZiIsIjE1LmYiLCIxNi52IiwiMTcuZiIsIjE4LnYiLCIxOS52IiwiMjEuZiIsIjIyLmYiLCIzMS52IiwiMzIuYyIsIjMzLnYiLCIzMy5jIiwiMzQudiIsIjM0LmMiLCIyNy52IiwiMzQudSIsIjM1LmMiLCIzNS52IiwiMzUudSIsIjM3LmMiLCIzNy52IiwiMzgudiIsIjQxLmMiLCI0MS51IiwiNDEudiJdLCJlbWFpbCI6IkxlVXllblRhbTE2NTgxMzk5ODIzOTFAbm9tYWlsLmNvbSIsInVzZXJuYW1lIjoiMDA1ODEzOTk4MiIsImZpcnN0bG9naW4iOmZhbHNlLCJjcmVhdGVkYnlhZG1pbiI6ZmFsc2UsInRva2VuZGVhY3RpdmV1c2VyIjoiZWREbzhiaWM4SnN5V3JmeGI1SE9vYy9TSHF5T21NOU0rWnlwWC9oNEhFaHkza0NIdXIwVkFwY0Nta1VjaHgxeCs4cStNcEJQbHV1TXdTR1NLcWc5TFE9PSIsInBsYXRmb3JtdHlwZSI6MCwiZGV2aWNlIjoiNGI5MGZmZmEtZDU2Mi00YmYwLWI2ZjYtY2JlYWIzOTc1MmViIiwic2lkIjoiM0VCODkwQ0QwQzY0MDREQ0UxRjk2QUQ1QUNFRDFBQTEiLCJpYXQiOjE2NTgxNTg3OTYsInNjb3BlIjpbIm9wZW5pZCIsInByb2ZpbGUiLCJlbWFpbCIsImdyYXBocWwtZ2F0ZXdheSIsImNvbW11bmljYXRpb24iLCJwZXJzb25hbCIsInNhbGUiLCJwb3N0Iiwic3VwcG9ydC1yZXF1ZXN0IiwiY29udGVudCIsIm1hc3Rlci1kYXRhIiwidHJhbnNhY3Rpb24iLCJJZGVudGl0eVNlcnZlckFwaSIsImNvbnRhY3QtdHJhZGluZyIsInN1YnNjcmlwdGlvbiIsImNvbnRyYWN0IiwiaW1hZ2UiLCJmaWxlIiwicGFydG5lcnMiLCJzb2NpYWwtbmV0d29yayIsImRvY3VtZW50Iiwic3VwcG9ydC1zZXJ2aWNlIiwiY3Jhd2xlciIsInBheW1lbnQiLCJjMmMtY29udGFjdC10cmFkaW5nIl0sImFtciI6WyJwd2QiXX0.J8gwJKKtvj6HnMnemGgaj-5YXJkwSCueXmfTH62vgi0IP3ydU9AKToJqTkHlhqsGhEOzkmiPqo3CbHX8tWW3xehuXNPazcIdkGgrE8UewVhhcQJVLYkP4KDBCeyc3CfQ-f9EC4mJqDRSP47_xX5E49FvQ3maVjSCsM4fhkCGl4h0Wun4_hx0EPz51bZvhARBmAD7APLMLJvJJcs7bfrTr4AnHih1wDaL__WhXWz2CQnkXoDukzfY4o_H2GFSUD_jvpoxbOxh4dRqtAt1zKEhwIcbDDeL3sSzpSQ3jlILBv7s1QMe4H-Qe5E0Ee9Gosr9B8PmgfGh1fgVoFw8qWG6IA",
};

Given(/^I am on page (.*)$/, async (pageUrl: string) => {
  await browser.url(pageUrl);
});
When(/^I make GET (.*) api call$/, async (endpoint: string) => {
  response = await request.get(endpoint);
});
Then(/^I validate the search result$/, async () => {
  api_statusCode = response.statusCode;
  api_resBody = JSON.stringify(response.body);
  console.log(api_resBody);
  expect(api_statusCode.toString()).toEqual("200");
  expect(api_resBody).toContain("janet.weaver@reqres.in");
  console.log(response);
});

When(/^I make POST (.*) api call$/, async (endpoint: string) => {
  response = await request.post(endpoint).set(header).send(payload);
});

Then(/^I validate the create user search result$/, async () => {
  api_statusCode = response.statusCode;
  api_resBody = JSON.stringify(response.body);
  expect(api_statusCode.toString()).toEqual("201");
  expect(api_resBody).toContain("test");
});

When(/^I make POST (.*) api call get token$/, async (endpoint: string) => {
  response = await request.post(endpoint).set(authed).send(bodies);
});

Then(/^I validate get token success$/, async () => {
  expect(api_statusCode.toString()).toEqual("200");
});
