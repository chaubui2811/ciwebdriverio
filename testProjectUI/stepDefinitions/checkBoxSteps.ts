import { Given, When } from "@wdio/cucumber-framework";
import CheckBoxPage from "../pageObjects/checkBoxPage/checkBoxPage";

Given(/^I am on ToolQA\/checkbox page$/, async () => {
  await CheckBoxPage.goTo("checkbox");
});
When(/^I test checkbox$/, async () => {
  await CheckBoxPage.verifyCheckBox();
});
