import { Given, When } from "@wdio/cucumber-framework";
import UploadPage from "../pageObjects/uploadPage/uploadPage";

Given(/^I am on gru99 upload page$/, async () => {
  await UploadPage.goTo("upload-download");
});

When(/^I upload file$/, async () => {
  await UploadPage.uploadFile();
});
