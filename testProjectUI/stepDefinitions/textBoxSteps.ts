import { Given, When, Then } from "@wdio/cucumber-framework";
import TextBoxPage from "../pageObjects/textBoxPage/textBoxPage";
import { environmentVariables } from "../config/envManagement";

Given(/^I am on ToolQA\/text-box page$/, async () => {
  await TextBoxPage.goTo("text-box");
});
When(/^I fill all personal information$/, async () => {
  await TextBoxPage.TypeText(
    environmentVariables.firstName,
    environmentVariables.jobTitle,
    environmentVariables.lastName,
    "nguyen thi that"
  );
});
When(/^I click on Submit button$/, async () => {
  await TextBoxPage.ClickSubmit();
});
Then(/^My info display on list below$/, async () => {
  await console.log("aa");
});
