import { Given, When } from "@wdio/cucumber-framework";
import ButtonsPage from "../pageObjects/buttonPage/buttonsPage";

Given(/^I am on ToolQA\/buttons page$/, async () => {
  await ButtonsPage.goTo("https://demoqa.com/buttons");
});
When(/^I double click on \[double click me button\]$/, async () => {
  await ButtonsPage.verifyTestDisplayed();
  // await ButtonsPage.DoubleClick();
});
When(/^I right click on \[right click me button\]$/, async () => {
  await ButtonsPage.rightClick();
});
When(/^I click on \[click me button\]$/, async () => {
  await ButtonsPage.leftClick();
});
