import { Given, When, Then } from "@wdio/cucumber-framework";
import WelcomePage from "../pageObjects/cmsKiwimi/welcomePage/welcomePage";
import DashboardPage from "../pageObjects/cmsKiwimi/dashboardPage/dashboardPage";

Given(/^I am on the Kiwimi Admin page$/, async () => {
  await WelcomePage.goTo("/");
});
When(
  /^I login with (.*) and (.*)$/,
  async (username: string, password: string) => {
    await WelcomePage.login(username, password);
  }
);
Then(
  /^I should see a flash message saying (.*)$/,
  async (welcomeText: string) => {
    await DashboardPage.checkMessage(welcomeText);
  }
);
