export class WelcomePageRepo {
  static get usernameTxt() {
    return $("//input[@id='mui-1']");
  }

  static get passwordTxt() {
    return $("//input[@id='mui-2']");
  }

  static get loginBtn() {
    return $("//button[normalize-space()='Sign in']");
  }
}

export default new WelcomePageRepo();
