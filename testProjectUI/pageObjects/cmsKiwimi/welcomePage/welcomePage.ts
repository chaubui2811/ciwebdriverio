import { BasePage } from "../../../../framework/ui/basePage";
import { WelcomePageRepo } from "./welcomePageRepo";

class WelcomePage extends BasePage {
  constructor() {
    super();
  }
  async goTo(path: string): Promise<string> {
    return super.goTo(path);
  }

  public async login(username: string, password: string) {
    await this.webUI.waitManagement.staticWait(4);
    await this.webUI.verification.verifyElementIsExisting(
      WelcomePageRepo.usernameTxt
    );
    await this.webUI.verification.verifyElementIsExisting(
      WelcomePageRepo.passwordTxt
    );
    await this.webUI.verification.verifyElementIsExisting(
      WelcomePageRepo.loginBtn
    );
    await this.webUI.seleneAction.clearTextAndTypeText(
      WelcomePageRepo.usernameTxt,
      username
    );
    await this.webUI.seleneAction.clearTextAndTypeText(
      WelcomePageRepo.passwordTxt,
      password
    );
    await this.webUI.seleneAction.click(WelcomePageRepo.loginBtn);
  }
}

export default new WelcomePage();
