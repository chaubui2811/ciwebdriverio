import { BasePage } from "../../../../framework/ui/basePage";
import { DashboardPageRepo } from "./dashboardPageRepo";

class WelcomePage extends BasePage {
  constructor() {
    super();
  }

  public async checkMessage(message: string) {
    // await this.webUI.waitManagement.staticWait(4);
    await this.webUI.verification.verifyElementToHaveTextContaining(
      DashboardPageRepo.welcomeText,
      message
    );
  }
}

export default new WelcomePage();
