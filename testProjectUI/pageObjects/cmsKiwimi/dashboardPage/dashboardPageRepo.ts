export class DashboardPageRepo {
  static get welcomeText() {
    return $("//span[@class='jss13']");
  }
}

export default new DashboardPageRepo();
