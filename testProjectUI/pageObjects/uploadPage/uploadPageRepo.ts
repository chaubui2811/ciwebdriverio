export class UploadPageRepo {
  static get uploadFileBtn() {
    return $("//input[@id='uploadFile']");
  }

  static get uploadSuccessMgs() {
    return $("//p[@id='uploadedFilePath']");
  }
}

export default new UploadPageRepo();
