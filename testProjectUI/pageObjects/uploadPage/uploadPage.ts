import { BasePage } from "../../../framework/ui/basePage";
import { UploadPageRepo } from "./uploadPageRepo";

export class UploadPage extends BasePage {
  // public goTo = async (path: string) => await this.goTo(path);
  constructor() {
    super();
  }

  async goTo(path: string): Promise<string> {
    return super.goTo(path);
  }

  public uploadFile = async () => {
    await this.webUI.actionBuilder.uploadFile(
      UploadPageRepo.uploadFileBtn,
      "/Users/hungle/Workspace/images.jpeg"
    );
    await this.webUI.verification.verifyElementToHaveTextContaining(
      UploadPageRepo.uploadSuccessMgs,
      "fakepath"
    );
  };
}

export default new UploadPage();
