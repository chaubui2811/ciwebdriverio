export class TextBoxPageRepo {
  static get fullNameTxt() {
    return $("//input[@id='userName']");
  }

  static get eMailTxt() {
    return $("//input[@id='userEmail']");
  }

  static get currentAddressTxt() {
    return $("//textarea[@id='currentAddress']");
  }

  static get permanentAddressTxt() {
    return $("//textarea[@id='permanentAddress']");
  }

  static get submitBtn() {
    return $("//button[@id='submit']");
  }
}

export default new TextBoxPageRepo();
