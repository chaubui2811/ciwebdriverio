import { BasePage } from "../../../framework/ui/basePage";
import { TextBoxPageRepo } from "./textBoxPageRepo";

class TextBoxPage extends BasePage {
  constructor() {
    super();
  }

  async goTo(path: string): Promise<string> {
    return super.goTo(path);
  }

  public async TypeText(
    fullNameTxt: string,
    eMailTxt: string,
    currentAddressTxt: string,
    permanentAddressTxt: string
  ) {
    await this.webUI.seleneAction.clearTextAndTypeText(
      TextBoxPageRepo.fullNameTxt,
      fullNameTxt
    );
    await this.webUI.seleneAction.typeTextWithoutClearText(
      TextBoxPageRepo.eMailTxt,
      eMailTxt
    );
    await this.webUI.seleneAction.typeTextWithDelay(
      TextBoxPageRepo.currentAddressTxt,
      currentAddressTxt,
      2
    );
    await this.webUI.seleneAction.clearTextAndTypeText(
      TextBoxPageRepo.permanentAddressTxt,
      permanentAddressTxt
    );
    await this.webUI.waitManagement.staticWait(2);
    await this.webUI.seleneAction.clearText(
      TextBoxPageRepo.permanentAddressTxt
    );
  }

  public async ClickSubmit() {
    await this.webUI.seleneAction.click(TextBoxPageRepo.submitBtn);
    await this.webUI.waitManagement.staticWait(10);
  }
}

export default new TextBoxPage();
