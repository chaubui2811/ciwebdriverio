import { BasePage } from "../../../framework/ui/basePage";
import { CheckBoxPageRepo } from "./checkBoxPageRepo";

export class CheckBoxPage extends BasePage {
  constructor() {
    super();
  }

  public goTo = (path) => super.goTo(path);

  public async verifyCheckBox() {
    await this.webUI.seleneAction.click(CheckBoxPageRepo.homeCheckBox);
    await this.webUI.verification.verifyElementToHaveAttributeValueContain(
      CheckBoxPageRepo.homeCheckBox,
      "class",
      "check"
    );
    await this.webUI.seleneAction.click(CheckBoxPageRepo.expandAllBtn);
    await this.webUI.waitManagement.staticWait(4);
    await this.webUI.seleneAction.click(CheckBoxPageRepo.desktopCheckBox);
    await this.webUI.seleneAction.click(CheckBoxPageRepo.notesCheckBox);
    await this.webUI.seleneAction.click(CheckBoxPageRepo.documentsCheckBox);
    await this.webUI.waitManagement.staticWait(4);
  }
}

export default new CheckBoxPage();
