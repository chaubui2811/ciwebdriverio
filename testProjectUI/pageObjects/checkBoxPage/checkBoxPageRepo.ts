export class CheckBoxPageRepo {
  static get homeCheckBox() {
    return $("//label[@for='tree-node-home']//span[@class='rct-checkbox']");
  }

  static get expandAllBtn() {
    return $("//button[@title='Expand all']");
  }

  static get desktopCheckBox() {
    return $("//label[@for='tree-node-desktop']//span[@class='rct-checkbox']");
  }

  static get notesCheckBox() {
    return $("//label[@for='tree-node-notes']//span[@class='rct-checkbox']");
  }

  static get documentsCheckBox() {
    return $(
      "//label[@for='tree-node-documents']//span[@class='rct-checkbox']"
    );
  }
}

export default new CheckBoxPageRepo();
