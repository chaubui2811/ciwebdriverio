export class ButtonsPageRepo {
  static get doubleClickBtn() {
    return $("//button[@id='doubleClickBtn']");
  }

  static get doubleClickMessage() {
    return $("//p[@id='doubleClickMessage']");
  }

  static get rightClickBtn() {
    return $("//button[@id='rightClickBtn']");
  }

  static get rightClickMessage() {
    return $("//p[@id='rightClickMessage']");
  }

  static get dynamicClickMessage() {
    return $("//p[@id='dynamicClickMessage']");
  }

  static get clickMeBtn() {
    return $(
      "//button[@id='rightClickBtn']//parent::div/following-sibling::div/button[@type='button']"
    );
  }

  static get invisibleElement() {
    return $("//button[@id='rightClickBtnAss']");
  }
}

export default new ButtonsPageRepo();
