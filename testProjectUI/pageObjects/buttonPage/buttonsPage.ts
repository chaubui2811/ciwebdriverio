import { BasePage } from "../../../framework/ui/basePage";
import { ButtonsPageRepo } from "./buttonsPageRepo";

class ButtonsPage extends BasePage {
  constructor() {
    super();
  }

  public async verifyTestDisplayed() {
    await this.webUI.verification.verifyElementIsExisting(
      ButtonsPageRepo.doubleClickBtn
    );
    await this.webUI.verification.verifyElementIsExisting(
      ButtonsPageRepo.rightClickBtn
    );
    await this.webUI.verification.verifyElementIsExisting(
      ButtonsPageRepo.clickMeBtn
    );
    await this.webUI.verification.verifyElementIsClickable(
      ButtonsPageRepo.clickMeBtn
    );
    await this.webUI.verification.verifyElementIsExisting(
      ButtonsPageRepo.clickMeBtn
    );
    await this.webUI.checkState.isClickable(ButtonsPageRepo.doubleClickBtn);
    await this.webUI.checkState.isDisplayed(ButtonsPageRepo.doubleClickBtn);
    await this.webUI.checkState.isExisting(ButtonsPageRepo.doubleClickBtn);
    await this.webUI.verification.verifyElementInvisible(
      ButtonsPageRepo.invisibleElement
    );
  }

  public async doubleClick() {
    await this.webUI.seleneAction.doubleClick(ButtonsPageRepo.doubleClickBtn);
    await this.webUI.waitManagement.staticWait(2);
    await this.webUI.verification.verifyElementToHaveText(
      ButtonsPageRepo.doubleClickMessage,
      "You have done a double click"
    );
    await this.webUI.waitManagement.staticWait(2);
  }

  public async rightClick() {
    await this.webUI.seleneAction.rightClick(ButtonsPageRepo.rightClickBtn);
    await this.webUI.waitManagement.staticWait(2);
    await this.webUI.verification.verifyElementToHaveText(
      ButtonsPageRepo.rightClickMessage,
      "You have done a right click"
    );
    await this.webUI.waitManagement.staticWait(2);
  }

  public async leftClick() {
    await this.webUI.waitManagement.waitForElementToExist(
      ButtonsPageRepo.clickMeBtn,
      10
    );
    await this.webUI.seleneAction.click(ButtonsPageRepo.clickMeBtn);
    await this.webUI.waitManagement.staticWait(2);
    await this.webUI.verification.verifyElementToHaveText(
      ButtonsPageRepo.dynamicClickMessage,
      "You have done a dynamic click"
    );
    await this.webUI.waitManagement.staticWait(2);
  }
}

export default new ButtonsPage();
