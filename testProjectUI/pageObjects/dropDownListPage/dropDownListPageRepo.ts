export class DropDownListPageRepo {
  static get oldStyleDropdown() {
    return $("//select[@id='oldSelectMenu']");
  }
}

export default new DropDownListPageRepo();
