import { BasePage } from "../../../framework/ui/basePage";
import { DropDownListPageRepo } from "./dropDownListPageRepo";

export class DropDownListPage extends BasePage {
  constructor() {
    super();
  }

  async goTo(path: string): Promise<string> {
    return super.goTo(path);
  }

  public async selectOldStyleDropdownList() {
    await DropDownListPageRepo.oldStyleDropdown.click();
    await this.webUI.waitManagement.staticWait(1);
    await DropDownListPageRepo.oldStyleDropdown.selectByAttribute(
      "value",
      "Blue"
    );
    await this.webUI.waitManagement.staticWait(1);
    await DropDownListPageRepo.oldStyleDropdown.click();
    await this.webUI.waitManagement.staticWait(1);
    await DropDownListPageRepo.oldStyleDropdown.selectByIndex(4);
    await this.webUI.waitManagement.staticWait(1);
  }
}

export default new DropDownListPage();
