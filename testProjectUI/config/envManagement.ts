import envVariables from "./envVariables.json";

export let environmentVariables;

if ("QA" === process.env.ENV) {
  environmentVariables = envVariables.QA;
} else if ("UAT" === process.env.ENV) {
  environmentVariables = envVariables.UAT;
} else if ("STG" === process.env.ENV) {
  environmentVariables = envVariables.STG;
} else {
  environmentVariables = envVariables.SB;
}
