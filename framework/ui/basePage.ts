import { Container } from "typedi";
import { WebUI } from "./helper/webUI";
import { Logger } from "../helper/logger/logger";

export abstract class BasePage {
  private readonly MSG_GO_TO_URL: string = "Go to URL: ";
  protected readonly webUI: WebUI;
  protected readonly logger: Logger;

  protected constructor() {
    this.webUI = Container.get(WebUI);
    this.logger = Container.get(Logger);
  }

  public async goTo(path: string) {
    try {
      await this.logger.logInfo(this.MSG_GO_TO_URL + `${path}`);
      return browser.url(`${path}`);
    } catch (e) {
      await this.logger.logError(e.message);
    }
  }
}
