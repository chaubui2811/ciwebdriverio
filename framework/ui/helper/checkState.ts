import { ChainablePromiseElement } from "webdriverio";
import { WaitManagement } from "./waitManagement";
import { Logger } from "../../helper/logger/logger";
import { LogConstant } from "../../helper/logger/logConstant";
import { Inject, Service } from "typedi";

@Service()
export class CheckState {
  @Inject()
  private waitManagement: WaitManagement;

  @Inject()
  private logger: Logger;

  /**
   * Return true
   * NOTE: it doesn't work in mobile app native context
   * @param element
   * @param timeOutInSecond
   * @constructor
   */
  public isClickable = async (
    element: ChainablePromiseElement<WebdriverIO.Element>,
    timeOutInSecond?: number
  ): Promise<boolean> => {
    await this.waitManagement.waitForDisplayed(element, timeOutInSecond);
    let isClickable: boolean;
    try {
      isClickable = await element.isClickable();
      await this.logger.logInfo(
        LogConstant.MGS_ELEMENT_IS_CLICKABLE + `${await element.selector}`
      );
      return isClickable;
    } catch (e) {
      return isClickable;
    }
  };

  public isDisplayed = async (
    element: ChainablePromiseElement<WebdriverIO.Element>,
    timeOutInSecond?: number
  ): Promise<boolean> => {
    await this.waitManagement.waitForDisplayed(element, timeOutInSecond);
    let isDisplayed: boolean;
    try {
      isDisplayed = await element.isDisplayed();
      await this.logger.logInfo(
        LogConstant.MGS_ELEMENT_IS_DISPLAYED + `${await element.selector}`
      );
      return isDisplayed;
    } catch (e) {
      return isDisplayed;
    }
  };

  public isInvisible = async (
    element: ChainablePromiseElement<WebdriverIO.Element>
  ): Promise<boolean> => {
    let isInvisible: boolean;
    try {
      isInvisible = await element.isDisplayed();
      await this.logger.logInfo(
        LogConstant.MGS_ELEMENT_IS_INVISIBLE + `${await element.selector}`
      );
      return !isInvisible;
    } catch (e) {
      return true;
    }
  };

  public isEnabled = async (
    element: ChainablePromiseElement<WebdriverIO.Element>,
    timeOutInSecond?: number
  ): Promise<boolean> => {
    await this.waitManagement.waitForDisplayed(element, timeOutInSecond);
    let isEnabled: boolean;
    try {
      isEnabled = await element.isEnabled();
      await this.logger.logInfo(
        LogConstant.MGS_ELEMENT_IS_ENABLE + `${await element.selector}`
      );
      return isEnabled;
    } catch (e) {
      return isEnabled;
    }
  };

  /**
   * Return true if the selected element matches with the provided one.
   * @param element
   * @param sameElement
   * @param timeOutInSecond
   * @constructor
   */
  public isEqual = async (
    element: ChainablePromiseElement<WebdriverIO.Element>,
    sameElement: ChainablePromiseElement<WebdriverIO.Element>,
    timeOutInSecond?: number
  ): Promise<boolean> => {
    await this.waitManagement.waitForDisplayed(element, timeOutInSecond);
    await this.waitManagement.waitForDisplayed(sameElement, timeOutInSecond);
    let isEqual: boolean;
    try {
      isEqual = await element.isEqual(await sameElement);
      await this.logger.logInfo(
        LogConstant.MGS_ELEMENT_IS_EQUAL +
          `${await element.selector} and ${sameElement.selector}`
      );
      return isEqual;
    } catch (e) {
      return isEqual;
    }
  };

  public isExisting = async (
    element: ChainablePromiseElement<WebdriverIO.Element>,
    timeOutInSecond?: number
  ): Promise<boolean> => {
    await this.waitManagement.waitForDisplayed(element, timeOutInSecond);
    let isExisting: boolean;
    try {
      isExisting = await element.isExisting();
      await this.logger.logInfo(
        LogConstant.MGS_ELEMENT_IS_EXISTING + `${await element.selector}`
      );
      return isExisting;
    } catch (e) {
      return isExisting;
    }
  };

  public isSelected = async (
    element: ChainablePromiseElement<WebdriverIO.Element>,
    timeOutInSecond?: number
  ): Promise<boolean> => {
    await this.waitManagement.waitForDisplayed(element, timeOutInSecond);
    let isSelected: boolean;
    try {
      isSelected = await element.isSelected();
      await this.logger.logInfo(
        LogConstant.MGS_ELEMENT_IS_SELECTED + `${await element.selector}`
      );
      return isSelected;
    } catch (e) {
      return isSelected;
    }
  };
}
