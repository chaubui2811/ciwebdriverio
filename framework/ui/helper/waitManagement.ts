import { ChainablePromiseElement } from "webdriverio";
import { Inject, Service } from "typedi";
import { Logger } from "../../helper/logger/logger";
import { LogConstant } from "../../helper/logger/logConstant";

@Service()
export class WaitManagement {
  @Inject()
  private logger: Logger;

  public async waitForElementToExist(
    element: ChainablePromiseElement<WebdriverIO.Element>,
    timeOutInSecond?: number
  ) {
    if (timeOutInSecond != null) {
      try {
        await element.waitForExist({ timeout: timeOutInSecond * 1000 });
      } catch (e) {
        await this.logger.logInfo(e.message);
        throw e;
      }
    } else {
      try {
        await element.waitForExist();
      } catch (e) {
        await this.logger.logInfo(e.message);
        throw e;
      }
    }
  }

  async;

  public waitForElementToBeClickable = async (
    element: ChainablePromiseElement<WebdriverIO.Element>,
    timeOutInSecond?: number
  ) => {
    if (timeOutInSecond != null) {
      try {
        await element.waitForClickable({ timeout: timeOutInSecond * 1000 });
      } catch (e) {
        await this.logger.logInfo(e.message);
        throw e;
      }
    } else {
      try {
        await element.waitForClickable();
      } catch (e) {
        await this.logger.logInfo(e.message);
        throw e;
      }
    }
  };

  public waitForEnabled = async (
    element: ChainablePromiseElement<WebdriverIO.Element>,
    timeOutInSecond?: number
  ) => {
    if (timeOutInSecond != null) {
      try {
        await element.waitForEnabled({ timeout: timeOutInSecond * 1000 });
      } catch (e) {
        await this.logger.logInfo(e.message);
        throw e;
      }
    } else {
      try {
        await element.waitForEnabled();
      } catch (e) {
        await this.logger.logInfo(e.message);
        throw e;
      }
    }
  };

  public waitForDisplayed = async (
    element: ChainablePromiseElement<WebdriverIO.Element>,
    timeOutInSecond?: number
  ) => {
    if (timeOutInSecond != null) {
      try {
        await element.waitForDisplayed({ timeout: timeOutInSecond * 1000 });
      } catch (e) {
        await this.logger.logInfo(e.message);
        throw e;
      }
    } else {
      try {
        await element.waitForDisplayed();
      } catch (e) {
        await this.logger.logInfo(e.message);
        throw e;
      }
    }
  };

  public waitForPageLoad = async (timeOutInSecond?: number) => {
    if (timeOutInSecond != null) {
      try {
        await browser.waitUntil(
          () => browser.execute(() => document.readyState === "complete"),
          {
            timeout: timeOutInSecond * 1000,
            timeoutMsg: "Waiting for page load ready",
          }
        );
      } catch (e) {
        await this.logger.logInfo(e.message);
        throw e;
      }
    } else {
      try {
        await browser.waitUntil(() =>
          browser.execute(() => document.readyState === "complete")
        );
      } catch (e) {
        await this.logger.logInfo(e.message);
        throw e;
      }
    }
  };

  public staticWait = async (waitTimeInSecond?: number) => {
    await this.logger.logInfo(LogConstant.MSG_STATIC_WAIT + waitTimeInSecond);
    // eslint-disable-next-line wdio/no-pause
    await browser.pause(waitTimeInSecond * 1000);
  };
}
