import { Inject, Service } from "typedi";
import { SeleneAction } from "./actions/seleneAction";
import { WaitManagement } from "./waitManagement";
import { ActionBuilder } from "./actions/actionBuilder";
import { JsExecutor } from "./actions/jsExecutor";
import { SelectAction } from "./actions/selectAction";
import { CheckState } from "./checkState";
import { Verification } from "./verification";

@Service()
export class WebUI {
  @Inject()
  seleneAction: SeleneAction;
  @Inject()
  waitManagement: WaitManagement;
  @Inject()
  actionBuilder: ActionBuilder;
  @Inject()
  jsExecutor: JsExecutor;
  @Inject()
  selectAction: SelectAction;
  @Inject()
  checkState: CheckState;
  @Inject()
  verification: Verification;
}

// export default new SeleneAction();
