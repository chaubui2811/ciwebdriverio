import { ChainablePromiseElement } from "webdriverio";
import { WaitManagement } from "./waitManagement";
import { Logger } from "../../helper/logger/logger";
import { LogConstant } from "../../helper/logger/logConstant";
import { Inject, Service } from "typedi";
import { CheckState } from "./checkState";

@Service()
/**
 * The WDIO testrunner comes with a built in assertion library that allows you to make powerful assertions
 * on various aspects of the browser or elements within your (web) application.
 * It extends Jests Matchers functionality with additional, for e2e testing optimized, matchers
 */
export class Verification {
  @Inject()
  private waitManagement: WaitManagement;

  @Inject()
  private logger: Logger;

  @Inject()
  private CheckState: CheckState;

  public verifyElementIsDisplayed = async (
    element: ChainablePromiseElement<WebdriverIO.Element>,
    timeOutInSecond?: number
  ) => {
    await this.waitManagement.waitForDisplayed(element, timeOutInSecond);
    await this.logger.logInfo(
      LogConstant.MGS_VERIFY_ELEMENT_IS_DISPLAYED + `${await element.selector}`
    );
    // return expect(this.CheckState.IsDisplayed(element, timeOutInSecond));
    return expect(element).toBeDisplayed();
  };

  public verifyElementInvisible = async (
    element: ChainablePromiseElement<WebdriverIO.Element>
  ) => {
    await this.logger.logInfo(
      LogConstant.MGS_VERIFY_ELEMENT_IS_DISPLAYED + `${await element.selector}`
    );
    await this.CheckState.isInvisible(element);
    return expect(await this.CheckState.isInvisible(element)).toBe(true);
  };

  public verifyElementIsClickable = async (
    element: ChainablePromiseElement<WebdriverIO.Element>,
    timeOutInSecond?: number
  ) => {
    await this.waitManagement.waitForDisplayed(element, timeOutInSecond);
    await this.logger.logInfo(
      LogConstant.MGS_VERIFY_ELEMENT_IS_CLICKABLE + `${await element.selector}`
    );
    return expect(element).toBeClickable();
  };

  public verifyElementIsEnabled = async (
    element: ChainablePromiseElement<WebdriverIO.Element>,
    timeOutInSecond?: number
  ) => {
    await this.waitManagement.waitForDisplayed(element, timeOutInSecond);
    await this.logger.logInfo(
      LogConstant.MGS_VERIFY_ELEMENT_IS_ENABLE + `${await element.selector}`
    );
    return expect(element).toBeEnabled();
  };

  public verifyElementIsExisting = async (
    element: ChainablePromiseElement<WebdriverIO.Element>,
    timeOutInSecond?: number
  ) => {
    await this.waitManagement.waitForElementToExist(element, timeOutInSecond);
    await this.logger.logInfo(
      LogConstant.MGS_VERIFY_ELEMENT_IS_EXISTING + `${await element.selector}`
    );
    return expect(element).toExist();
  };

  public verifyElementIsSelected = async (
    element: ChainablePromiseElement<WebdriverIO.Element>,
    timeOutInSecond?: number
  ) => {
    await this.waitManagement.waitForDisplayed(element, timeOutInSecond);
    await this.logger.logInfo(
      LogConstant.MGS_VERIFY_ELEMENT_IS_SELECTED + `${await element.selector}`
    );
    return expect(element).toBeSelected();
  };

  public verifyElementToHaveTextContaining = async (
    element: ChainablePromiseElement<WebdriverIO.Element>,
    expectText: string,
    timeOutInSecond?: number
  ) => {
    await this.waitManagement.waitForDisplayed(element, timeOutInSecond);
    const actual = await element.getText();
    await this.logger.logInfo(
      LogConstant.MGS_VERIFY_ELEMENT_TO_HAVE_TEXT_CONTAIN +
        `actual: ${actual} expected: ${expectText}`
    );
    return expect(element).toHaveTextContaining(expectText);
  };

  public verifyElementToHaveText = async (
    element: ChainablePromiseElement<WebdriverIO.Element>,
    expectText: string,
    timeOutInSecond?: number
  ) => {
    await this.waitManagement.waitForDisplayed(element, timeOutInSecond);
    const actual = await element.getText();
    await this.logger.logInfo(
      LogConstant.MGS_VERIFY_ELEMENT_TO_HAVE_TEXT +
        `actual: ${actual} expected: ${expectText}`
    );
    return expect(element).toHaveText(expectText);
  };

  public verifyElementToHaveAttributeValue = async (
    element: ChainablePromiseElement<WebdriverIO.Element>,
    attributeValue: string,
    expectText: string,
    timeOutInSecond?: number
  ) => {
    await this.waitManagement.waitForDisplayed(element, timeOutInSecond);
    const actual = await element.getAttribute(attributeValue);
    await this.logger.logInfo(
      LogConstant.MGS_VERIFY_ELEMENT_TO_HAVE_ATTRIBUTE_VALUE +
        `actual: ${actual} expected: ${expectText}`
    );
    return expect(element).toHaveAttribute(attributeValue, expectText);
  };

  public verifyElementToHaveAttributeValueContain = async (
    element: ChainablePromiseElement<WebdriverIO.Element>,
    attributeValue: string,
    expectText: string,
    timeOutInSecond?: number
  ) => {
    await this.waitManagement.waitForDisplayed(element, timeOutInSecond);
    const actual = await element.getAttribute(attributeValue);
    await this.logger.logInfo(
      LogConstant.MGS_VERIFY_ELEMENT_TO_HAVE_ATTRIBUTE_VALUE_CONTAIN +
        `actual: ${actual} expected: ${expectText}`
    );
    return expect(element).toHaveAttributeContaining(
      attributeValue,
      expectText
    );
  };
}
