import { ChainablePromiseElement } from "webdriverio";
import { WaitManagement } from "../waitManagement";
import { Logger } from "../../../helper/logger/logger";
import { LogConstant } from "../../../helper/logger/logConstant";
import { Inject, Service } from "typedi";

@Service()
export class JsExecutor {
  @Inject()
  private waitManagement: WaitManagement;

  @Inject()
  private logger: Logger;

  public jsExecute = async (script: string) => {
    try {
      await this.logger.logInfo(LogConstant.MSG_JS_EXECUTE + script);
      return await browser.execute(script);
    } catch (e) {
      await this.logger.logError(e.message);
      throw e;
    }
  };

  public jsExecuteWithElement = async (
    script: string,
    element: ChainablePromiseElement<WebdriverIO.Element>
  ) => {
    try {
      await this.logger.logInfo(
        LogConstant.MSG_JS_EXECUTE + script + " " + element.selector
      );
      return await browser.execute(script, element);
    } catch (e) {
      await this.logger.logError(e.message);
      throw e;
    }
  };

  public scrollIntoView = async (
    element: ChainablePromiseElement<WebdriverIO.Element>
  ): Promise<void> => {
    await this.waitManagement.waitForElementToExist(element);
    try {
      await this.logger.logInfo(LogConstant.MSG_SCROLL_INTO + element.selector);
      await element.scrollIntoView();
    } catch (e) {
      await this.logger.logError(e.message);
      throw e;
    }
  };
}
