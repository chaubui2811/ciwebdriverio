import { ChainablePromiseElement } from "webdriverio";
import { WaitManagement } from "../waitManagement";
import { Logger } from "../../../helper/logger/logger";
import { LogConstant } from "../../../helper/logger/logConstant";
import { Inject, Service } from "typedi";

@Service()
export class SelectAction {
  @Inject()
  private waitManagement: WaitManagement;

  @Inject()
  private logger: Logger;

  /**
   * Select option with a specific attribute value or name.
   * @param element
   * @param selectAttributeBy - value|name
   * @param attributeValue
   * @param timeOutInSecond
   * @constructor
   */
  public selectByAttribute = async (
    element: ChainablePromiseElement<WebdriverIO.Element>,
    selectAttributeBy: string,
    attributeValue: string | number,
    timeOutInSecond?: number
  ): Promise<void> => {
    await this.waitManagement.waitForElementToBeClickable(
      element,
      timeOutInSecond
    );
    try {
      await element.selectByAttribute(selectAttributeBy, attributeValue);
      await this.logger.logInfo(LogConstant.MGS_SELECT_BY_ATTRIBUTE);
    } catch (e) {
      await this.logger.logError(e.message);
      throw e;
    }
  };

  public selectByIndex = async (
    element: ChainablePromiseElement<WebdriverIO.Element>,
    index: number,
    timeOutInSecond?: number
  ): Promise<void> => {
    await this.waitManagement.waitForElementToBeClickable(
      element,
      timeOutInSecond
    );
    try {
      await element.selectByIndex(index);
      await this.logger.logInfo(LogConstant.MGS_SELECT_BY_INDEX);
    } catch (e) {
      await this.logger.logError(e.message);
      throw e;
    }
  };

  public selectByVisibleText = async (
    element: ChainablePromiseElement<WebdriverIO.Element>,
    text: string | number,
    timeOutInSecond?: number
  ): Promise<void> => {
    await this.waitManagement.waitForElementToBeClickable(
      element,
      timeOutInSecond
    );
    try {
      await element.selectByVisibleText(text);
      await this.logger.logInfo(LogConstant.MGS_SELECT_BY_Text);
    } catch (e) {
      await this.logger.logError(e.message);
      throw e;
    }
  };

  public deSelectByAttribute = async (
    element: ChainablePromiseElement<WebdriverIO.Element>,
    selectAttributeBy: string,
    attributeValue: string | number,
    timeOutInSecond?: number
  ): Promise<void> => {
    await this.waitManagement.waitForElementToBeClickable(
      element,
      timeOutInSecond
    );
    try {
      await element.selectByAttribute(selectAttributeBy, attributeValue);
      await this.logger.logInfo(LogConstant.MGS_DESELECT_BY_ATTRIBUTE);
    } catch (e) {
      await this.logger.logError(e.message);
      throw e;
    }
  };

  public deSelectByIndex = async (
    element: ChainablePromiseElement<WebdriverIO.Element>,
    index: number,
    timeOutInSecond?: number
  ): Promise<void> => {
    await this.waitManagement.waitForElementToBeClickable(
      element,
      timeOutInSecond
    );
    try {
      await element.selectByIndex(index);
      await this.logger.logInfo(LogConstant.MGS_DESELECT_BY_INDEX);
    } catch (e) {
      await this.logger.logError(e.message);
      throw e;
    }
  };

  public deSelectByVisibleText = async (
    element: ChainablePromiseElement<WebdriverIO.Element>,
    text: string | number,
    timeOutInSecond?: number
  ): Promise<void> => {
    await this.waitManagement.waitForElementToBeClickable(
      element,
      timeOutInSecond
    );
    try {
      await element.selectByVisibleText(text);
      await this.logger.logInfo(LogConstant.MGS_DESELECT_BY_Text);
    } catch (e) {
      await this.logger.logError(e.message);
      throw e;
    }
  };
}
