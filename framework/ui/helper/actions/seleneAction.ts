import { ChainablePromiseElement } from "webdriverio";
import { WaitManagement } from "../waitManagement";
import { Logger } from "../../../helper/logger/logger";
import { LogConstant } from "../../../helper/logger/logConstant";
import { Inject, Service } from "typedi";

@Service()
export class SeleneAction {
  @Inject()
  private waitManagement: WaitManagement;

  @Inject()
  private logger: Logger;

  //region Performance action
  public click = async (
    element: ChainablePromiseElement<WebdriverIO.Element>,
    timeOutInSecond?: number
  ): Promise<void> => {
    await this.waitManagement.waitForElementToBeClickable(
      element,
      timeOutInSecond
    );
    try {
      await element.click();
      await this.logger.logInfo(
        LogConstant.MSG_CLICK + `${await element.selector}`
      );
    } catch (e) {
      await this.logger.logError(e.message);
      throw e;
    }
  };

  public clearTextAndTypeText = async (
    element: ChainablePromiseElement<WebdriverIO.Element>,
    textInput: string,
    timeOutInSecond?: number
  ): Promise<void> => {
    await this.waitManagement.waitForElementToBeClickable(
      element,
      timeOutInSecond
    );
    try {
      await this.clearText(element, timeOutInSecond);
      await element.setValue(textInput);
      await this.logger.logInfo(LogConstant.MSG_TYPE_TEXT + textInput);
    } catch (e) {
      await this.logger.logError(e.message);
      throw e;
    }
  };

  public typeTextWithoutClearText = async (
    element: ChainablePromiseElement<WebdriverIO.Element>,
    textInput: string,
    timeOutInSecond?: number
  ): Promise<void> => {
    await this.waitManagement.waitForElementToBeClickable(
      element,
      timeOutInSecond
    );
    try {
      await element.addValue(textInput);
      await this.logger.logInfo(LogConstant.MSG_TYPE_TEXT + textInput);
    } catch (e) {
      await this.logger.logError(e.message);
      throw e;
    }
  };

  public typeTextWithDelay = async (
    element: ChainablePromiseElement<WebdriverIO.Element>,
    textInput: string,
    timeDelay = 1,
    timeOutInSecond?: number
  ): Promise<void> => {
    await this.waitManagement.waitForElementToBeClickable(
      element,
      timeOutInSecond
    );
    try {
      await this.logger.logInfo(
        LogConstant.MSG_TYPE_TEXT_WITH_DELAY + textInput + timeDelay
      );
      for (let i = 0; i < textInput.length; i++) {
        await this.typeTextWithoutClearText(
          element,
          textInput.charAt(i),
          timeOutInSecond
        );
        await this.waitManagement.staticWait(timeDelay);
      }
    } catch (e) {
      await this.logger.logError(e.message);
      throw e;
    }
  };

  public clearText = async (
    element: ChainablePromiseElement<WebdriverIO.Element>,
    timeOutInSecond?: number
  ): Promise<void> => {
    await this.waitManagement.waitForDisplayed(element, timeOutInSecond);
    try {
      await element.clearValue();
      await this.logger.logInfo(
        LogConstant.MSG_CLEAR_TEXT + `${await element.selector}`
      );
    } catch (e) {
      await this.logger.logError(e.message);
      throw e;
    }
  };

  public rightClick = async (
    element: ChainablePromiseElement<WebdriverIO.Element>,
    timeOutInSecond?: number
  ): Promise<void> => {
    await this.waitManagement.waitForDisplayed(element, timeOutInSecond);
    try {
      await element.click({ button: "right" });
      await this.logger.logInfo(
        LogConstant.MSG_RIGHT_CLICK + `${await element.selector}`
      );
    } catch (e) {
      await this.logger.logError(e.message);
      throw e;
    }
  };

  public doubleClick = async (
    element: ChainablePromiseElement<WebdriverIO.Element>,
    timeOutInSecond?: number
  ): Promise<void> => {
    await this.waitManagement.waitForElementToBeClickable(
      element,
      timeOutInSecond
    );
    try {
      await element.doubleClick();
      await this.logger.logInfo(
        LogConstant.MSG_DOUBLE_CLICK + `${await element.selector}`
      );
    } catch (e) {
      await this.logger.logError(e);
      throw e;
    }
  };

  public dragAndDrop = async (
    fromElement: ChainablePromiseElement<WebdriverIO.Element>,
    toElement: ChainablePromiseElement<WebdriverIO.Element>,
    timeOutInSecond?: number
  ): Promise<void> => {
    await this.waitManagement.waitForElementToBeClickable(
      fromElement,
      timeOutInSecond
    );
    await this.waitManagement.waitForElementToBeClickable(
      toElement,
      timeOutInSecond
    );
    try {
      await fromElement.dragAndDrop(await toElement);
      await this.logger.logInfo(
        LogConstant.MSG_DRAG_AND_DROP +
          `${await fromElement.selector}` +
          "to" +
          `${await toElement.selector}`
      );
    } catch (e) {
      await this.logger.logError(e);
      throw e;
    }
  };

  //endregion

  //region Get action
  public async getText(
    element: ChainablePromiseElement<WebdriverIO.Element>,
    timeOutInSecond?: number
  ): Promise<string> {
    await this.waitManagement.waitForDisplayed(element, timeOutInSecond);
    try {
      await this.logger.logInfo(
        LogConstant.MSG_GET_TEXT + `${await element.selector}`
      );
      return await element.getText();
    } catch (e) {
      await this.logger.logError(e.message);
      throw e;
    }
  }

  public async getAttribute(
    element: ChainablePromiseElement<WebdriverIO.Element>,
    attributeName: string,
    timeOutInSecond?: number
  ): Promise<string> {
    await this.waitManagement.waitForDisplayed(element, timeOutInSecond);
    try {
      const attributeValue = element.getAttribute(attributeName);
      await this.logger.logInfo(LogConstant.MSG_GET_ATTRIBUTE + attributeValue);
      return attributeValue;
    } catch (e) {
      await this.logger.logError(e.message);
      throw e;
    }
  }

  public async getProperty(
    element: ChainablePromiseElement<WebdriverIO.Element>,
    propertyName: string,
    timeOutInSecond?: number
  ) {
    await this.waitManagement.waitForDisplayed(element, timeOutInSecond);
    try {
      const propertyValue = element.getProperty(propertyName);
      await this.logger.logInfo(
        LogConstant.MSG_GET_PROPERTY + `${propertyValue[Symbol.toStringTag]}`
      );
      return propertyValue;
    } catch (e) {
      await this.logger.logError(e.message);
      throw e;
    }
  }

  public async getValue(
    element: ChainablePromiseElement<WebdriverIO.Element>,
    timeOutInSecond?: number
  ): Promise<string> {
    await this.waitManagement.waitForDisplayed(element, timeOutInSecond);
    try {
      const elementValue = element.getValue();
      await this.logger.logInfo(LogConstant.MSG_GET_VALUE + elementValue);
      return elementValue;
    } catch (e) {
      await this.logger.logError(e.message);
      throw e;
    }
  }

  public getCSSProperty = async (
    element: ChainablePromiseElement<WebdriverIO.Element>,
    cSSPropertyName: string,
    timeOutInSecond?: number
  ) => {
    await this.waitManagement.waitForElementToBeClickable(
      element,
      timeOutInSecond
    );
    try {
      const cSSPropertyValue = element.getCSSProperty(cSSPropertyName);
      await this.logger.logInfo(
        LogConstant.MSG_GET_CSS_PROPERTY +
          `${cSSPropertyValue[Symbol.toStringTag]}`
      );
      return cSSPropertyValue;
    } catch (e) {
      await this.logger.logError(e.message);
      throw e;
    }
  };

  //endregion
}
