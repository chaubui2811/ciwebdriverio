import { ChainablePromiseElement } from "webdriverio";
import { WaitManagement } from "../waitManagement";
import { Logger } from "../../../helper/logger/logger";
import { LogConstant } from "../../../helper/logger/logConstant";
import { Inject, Service } from "typedi";

@Service()
export class ActionBuilder {
  @Inject()
  private waitManagement: WaitManagement;

  @Inject()
  private logger: Logger;

  //region Navigate
  public navigateTo = async (url: string): Promise<void> => {
    try {
      browser.navigateTo(url);
      await this.logger.logInfo(LogConstant.MGS_NAVIGATE_TO + url);
    } catch (e) {
      await this.logger.logError(e.message);
      throw e;
    }
  };

  public goBack = async (): Promise<void> => {
    try {
      browser.back();
      await this.logger.logInfo(LogConstant.MGS_GO_BACK);
    } catch (e) {
      await this.logger.logError(e.message);
      throw e;
    }
  };

  public goForward = async (): Promise<void> => {
    try {
      browser.forward();
      await this.logger.logInfo(LogConstant.MGS_GO_FORWARD);
    } catch (e) {
      await this.logger.logError(e.message);
      throw e;
    }
  };

  public refreshPage = async () => {
    try {
      browser.refresh();
      await this.logger.logInfo(LogConstant.MGS_REFRESH);
    } catch (e) {
      await this.logger.logError(e.message);
      throw e;
    }
  };

  //endregion

  //region WindowHandle
  public maximizeWindow = async (): Promise<void> => {
    try {
      await browser.maximizeWindow();
      await this.logger.logInfo(LogConstant.MGS_MAXIMIZE_WINDOW);
    } catch (e) {
      await this.logger.logError(e.message);
      throw e;
    }
  };

  public switchWindow = async (matcher: string): Promise<void> => {
    try {
      await browser.switchWindow(matcher);
      await this.logger.logInfo(LogConstant.MGS_SWITCH_WINDOW + matcher);
    } catch (e) {
      await this.logger.logError(e.message);
      throw e;
    }
  };

  public switchToFrame = async (frame): Promise<void> => {
    try {
      await browser.switchToFrame(frame);
      await this.logger.logInfo(LogConstant.MGS_SWITCH_TO_FRAME + frame.info);
    } catch (e) {
      await this.logger.logError(e.message);
      throw e;
    }
  };

  public closeWindow = async (): Promise<void> => {
    try {
      await this.logger.logInfo(LogConstant.MGS_CLOSE_WINDOW);
    } catch (e) {
      await this.logger.logError(e.message);
      throw e;
    }
  };

  //endregion

  /**
   * Modifier like Ctrl, Shift, Alt and Meta will stay pressed
   * , so you need to trigger them again to release them.
   *     await browser.keys('a')
   *     await browser.keys(['Meta', 'c'])
   * @param character
   * @constructor
   */
  public pressKey = async (
    character: string | Array<string>
  ): Promise<void> => {
    try {
      await browser.keys(character);
    } catch (e) {
      await this.logger.logInfo(e.message);
      throw e;
    }
  };

  public moveToElement = async (
    element: ChainablePromiseElement<WebdriverIO.Element>,
    timeOutInSecond?: number
  ): Promise<void> => {
    await this.waitManagement.waitForDisplayed(element, timeOutInSecond);
    try {
      await browser.moveToElement(await element.elementId);
    } catch (e) {
      await this.logger.logInfo(e.message);
      throw e;
    }
  };

  /**
   * That this command is only supported if you use a Selenium Hub or Chromedriver directly.
   * @param element
   * @param localPath
   * @param timeOutInSecond
   * @constructor
   */
  // public uploadFile = async (
  //   element: ChainablePromiseElement<WebdriverIO.Element>,
  //   localPath: string,
  //   timeOutInSecond?: number
  // ): Promise<void> => {
  //   await this.waitManagement.waitForDisplayed(element, timeOutInSecond);
  //   try {
  //     const remoteFilePath = await browser.uploadFile(localPath);
  //     await element.setValue(remoteFilePath);
  //     await this.logger.logInfo(LogConstant.MGS_UPLOAD_FILE);
  //   } catch (e) {
  //     await this.logger.logError(e.message);
  //     throw e;
  //   }
  // };

  public uploadFile = async (
    element: ChainablePromiseElement<WebdriverIO.Element>,
    localPath: string,
    timeOutInSecond?: number
  ): Promise<void> => {
    await this.waitManagement.waitForDisplayed(element, timeOutInSecond);
    try {
      await element.setValue(localPath);
      await this.logger.logInfo(LogConstant.MGS_UPLOAD_FILE);
    } catch (e) {
      await this.logger.logError(e.message);
      throw e;
    }
  };
}
