import { RequestHeaders } from "./requestHeaders";
import { RequestMethod } from "./requestMethod";
import { Container } from "typedi";
import authed from "../../testProjectUI/testData/authen.json";

export class BaseUtil {
  protected readonly requestHeaders: RequestHeaders;
  protected readonly requestMethod: RequestMethod;

  protected constructor() {
    this.requestHeaders = Container.get(RequestHeaders);
    this.requestMethod = Container.get(RequestMethod);
  }

  public getAccessToken = async (
    endpoint: string,
    userName = "",
    password = ""
  ) => {
    const authed = {
      userName: userName,
      password: password,
    };
    const response = await this.requestMethod.postRequest(
      null,
      endpoint,
      authed
    );
    return JSON.stringify(response.body);
  };
}
