import supertest from "supertest";
import { Inject, Service } from "typedi";
import { RequestHeaders } from "./requestHeaders";
import { environmentVariables } from "../../testProjectUI/config/envManagement";

@Service()
export class RequestMethod {
  @Inject()
  private requestHeaders: RequestHeaders;
  public request = supertest(environmentVariables.baseUrl);

  public getRequest = async (accessToken: string | null, endpoint: string) => {
    const response = await this.request
      .get(endpoint)
      .set(this.requestHeaders.setHeaders(accessToken));
    return response;
  };

  public postRequest = async (
    accessToken: string | null,
    endpoint: string,
    reBody: string | object | JSON
  ) => {
    const response = await this.request
      .post(endpoint)
      .set(this.requestHeaders.setHeaders(accessToken))
      .send(reBody);
    return response;
  };

  public putRequest = async (
    accessToken: string | null,
    endpoint: string,
    reBody: string | object | JSON
  ) => {
    const response = await this.request
      .put(endpoint)
      .set(this.requestHeaders.setHeaders(accessToken))
      .send(reBody);
    return response;
  };

  public deleteRequest = async (
    accessToken: string | null,
    endpoint: string,
    reBody: string | object | JSON
  ) => {
    const response = await this.request
      .delete(endpoint)
      .set(this.requestHeaders.setHeaders(accessToken))
      .send(reBody);
    return response;
  };
}
