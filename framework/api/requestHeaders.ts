import { Service } from "typedi";

@Service()
export class RequestHeaders {
  public setHeaders = async (accessToken: string | null) => {
    if (accessToken === null) {
      return {
        "content-type": "application/json",
        Authorization: `Bearer ${accessToken}`,
      };
    } else {
      return {
        "content-type": "application/json",
      };
    }
  };
}
