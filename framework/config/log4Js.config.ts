import { configure, getLogger } from "log4js";

configure({
  appenders: {
    application: {
      type: "console",
    },

    file: {
      type: "file",

      filename: "testOutput/logger/TestRun.log",

      compression: true,

      maxLogSize: 10485760,

      backups: 100,
    },
  },

  categories: {
    default: {
      appenders: ["application", "file"],
      level: "info",
    },
  },
});

export const logger = getLogger();
