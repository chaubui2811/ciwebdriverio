export class LogConstant {
  //Wait
  static readonly MSG_STATIC_WAIT: string = " Waiting in seconds ";
  //Action
  static readonly MSG_TYPE_TEXT: string = "Input text ";
  static readonly MSG_CLICK: string = "Click on element ";
  static readonly MSG_RIGHT_CLICK: string = "Right Click on element ";
  static readonly MSG_DOUBLE_CLICK: string = "Double Click on element ";
  static readonly MSG_DRAG_AND_DROP: string = "Drag and Drop element ";
  static readonly MSG_CLEAR_TEXT: string = "Clear text on element ";
  static readonly MSG_GET_TEXT: string = "Get text of element ";
  static readonly MSG_TYPE_TEXT_WITH_DELAY: string =
    "Type text on Element with delay time ";
  static readonly MSG_GET_ATTRIBUTE: string = "Attribute value gotten ";
  static readonly MSG_GET_CSS_PROPERTY: string = "CSS Property value gotten ";
  static readonly MSG_GET_PROPERTY: string = "Property value gotten ";
  static readonly MSG_GET_VALUE: string = "Element value gotten ";
  static readonly MSG_JS_EXECUTE: string = "Execute a JS ";
  static readonly MSG_SCROLL_INTO: string = "Scroll page to Element ";
  static readonly MGS_SWITCH_WINDOW: string = "Switch to window with url ";
  static readonly MGS_SWITCH_TO_FRAME: string = "Switch to frame ";
  static readonly MGS_MAXIMIZE_WINDOW: string = "Maximize Browser window";
  static readonly MGS_NAVIGATE_TO: string = "Navigate to url ";
  static readonly MGS_GO_BACK: string = "Browser - go backward ";
  static readonly MGS_GO_FORWARD: string = "Browser - go forward ";
  static readonly MGS_REFRESH: string = "Browser - refresh page ";
  static readonly MGS_CLOSE_WINDOW: string = "Close current window ";
  static readonly MGS_UPLOAD_FILE: string = "Upload local file ";
  static readonly MGS_SELECT_BY_ATTRIBUTE: string =
    "Selecting the element option by attribute: ";
  static readonly MGS_SELECT_BY_INDEX: string =
    "Select option with a specific index: ";
  static readonly MGS_SELECT_BY_Text: string =
    "Select option with displayed text: ";
  static readonly MGS_DESELECT_BY_ATTRIBUTE: string =
    "Deselect element option by attribute: ";
  static readonly MGS_DESELECT_BY_INDEX: string =
    "Deselect element option with a specific index: ";
  static readonly MGS_DESELECT_BY_Text: string =
    "Deselect element option with displayed text: ";
  static readonly MGS_ELEMENT_IS_CLICKABLE: string =
    "Checking if Element which is clickable or not: ";
  static readonly MGS_ELEMENT_IS_DISPLAYED: string =
    "Checking if Element which is displayed or not: ";
  static readonly MGS_ELEMENT_IS_INVISIBLE: string =
    "Checking if Element which is invisible or not: ";
  static readonly MGS_ELEMENT_IS_ENABLE: string =
    "Checking if Element which is enable or not: ";
  static readonly MGS_ELEMENT_IS_EQUAL: string =
    "Checking if Element selected element matches with the provided one or not: ";
  static readonly MGS_ELEMENT_IS_EXISTING: string =
    "Checking if Element which is Existing or not: ";
  static readonly MGS_ELEMENT_IS_SELECTED: string =
    "Checking if Element which is selected or not: ";
  static readonly MGS_VERIFY_ELEMENT_IS_DISPLAYED: string =
    "Verify if Web Element which is displayed or not: ";
  static readonly MGS_VERIFY_ELEMENT_IS_INVISIBLE: string =
    "Verify if Web Element which is invisible or not: ";
  static readonly MGS_VERIFY_ELEMENT_IS_CLICKABLE: string =
    "Verify if Web Element which is clickable or not: ";
  static readonly MGS_VERIFY_ELEMENT_IS_ENABLE: string =
    "Verify if Web Element which is enable or not: ";
  static readonly MGS_VERIFY_ELEMENT_IS_EXISTING: string =
    "Verify if element which is existing or not: ";
  static readonly MGS_VERIFY_ELEMENT_IS_SELECTED: string =
    "Verify if element which is selected or not: ";
  static readonly MGS_VERIFY_ELEMENT_TO_HAVE_TEXT: string =
    "Verify if Element To Have Text: ";
  static readonly MGS_VERIFY_ELEMENT_TO_HAVE_TEXT_CONTAIN: string =
    "Verify if element to have text contains: ";
  static readonly MGS_VERIFY_ELEMENT_TO_HAVE_ATTRIBUTE_VALUE: string =
    "Verify if an element has a certain attribute with a specific value: ";
  static readonly MGS_VERIFY_ELEMENT_TO_HAVE_ATTRIBUTE_VALUE_CONTAIN: string =
    "Verify if an element has a certain attribute with a specific value contain: ";
}
