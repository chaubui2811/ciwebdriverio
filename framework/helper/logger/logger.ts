import report from "@wdio/allure-reporter";
import { logger } from "../../config/log4Js.config";
import { Service } from "typedi";

@Service()
export class Logger {
  public logInfo = async (logMes) => {
    await report.addStep(logMes);
    await logger.info(logMes);
  };

  public logError = async (logMes) => {
    await report.addStep(logMes);
    await logger.error(logMes);
  };

  public logWarning = async (logMes) => {
    await report.addStep(logMes);
    await logger.warn(logMes);
  };

  public logFatal = async (logMes) => {
    await report.addStep(logMes);
    await logger.fatal(logMes);
  };
}
